{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Options.Applicative
import qualified Data.ByteString.Lazy as ByteString
import Network.HTTP.Conduit
import Network.HTTP.Simple ( addRequestHeader, getRequestHeader )
import Data.Aeson
import Data.Aeson.Encode.Pretty
import Database
import Arxiv
import Doi
import Data.Maybe
import Data.List
import qualified Data.ByteString.Lazy as ByteString
import Data.ByteString.Lazy (ByteString)
import Data.String
import System.Directory

data Instruction = Init
                 | Add FilePath Paper
                 | AddAuto FilePath Source [String]
                 | Get FilePath Source [String]
                 | Remove FilePath
                 | Move FilePath FilePath
                 | Meta FilePath
                 | ListText (Paper -> Bool)
                 | ListColors (Paper -> Bool)
                 | Set FilePath (Paper -> Paper)

cli :: Parser Instruction
cli = subparser
      (  command "init" (info (init     <**> helper)
                              (progDesc "Initialize a pap index in the current directory"))
      <> command "add"  (info (add  <**> helper)
                              (progDesc "Add a paper to the index"))
      <> command "get"  (info (get  <**> helper)
                              (progDesc "Download a paper and add to the index"))
      <> command "rm"   (info (rm   <**> helper)
                              (progDesc "Remove a paper from the index"))
      <> command "mv"   (info (mv   <**> helper)
                              (progDesc "Rename paper in the index (does not move file)"))
      <> command "set"  (info (set  <**> helper)
                              (progDesc "Set metadata for paper"))
      <> command "meta" (info (meta <**> helper)
                              (progDesc "Get metadata for paper"))
      <> command "ls"   (info (list <**> helper)
                              (progDesc "List papers"))
      )
  where
    init = pure Init
    file = argument str (  metavar "FILE"
                        <> action "file"
                        )
    -- TODO: Move to its own function
    add = do
        path <- file
        lambda <- addAuto <|> addManual
        pure $ lambda path
    addAuto = do
        flag' ()
            (  long "auto"
            <> help "Fetch metadata automatically"
            )
        source <-
          (    Arxiv
            <$> strOption
                    (  long "arxiv"
                    <> metavar "ARXIV ID"
                    <> help "Specify ArXiV source to fetch metadata from (for --auto)"
                    )
          ) <|>
          (     DOI
            <$> strOption
                    (  long "doi"
                    <> metavar "DOI"
                    <> help "Specify DOI source to fetch metadata from (for --auto)"
                    )
          )
        tags <- many $ strOption
            (  long "tag"
            <> short 'T'
            <> metavar "TAG"
            <> help "Specify a tag (use multiple times for multiple tags) (for --auto)"
            )
        pure $ \path -> AddAuto path source tags
    addManual = do
        title <- strOption
            (  long "title"
            <> short 't'
            <> metavar "TITLE"
            <> help "Specify title of paper"
            )
        authors <- some $ strOption
            (  long "author"
            <> short 'a'
            <> metavar "NAME"
            <> help "Specify an author (use multiple times for multiple authors)"
            )
        year <- option auto
            (  long "year"
            <> short 'y'
            <> metavar "YEAR"
            <> help "Specify year of paper"
            )
        tags <- many $ strOption
            (  long "tag"
            <> short 'T'
            <> metavar "TAG"
            <> help "Specify a tag (use multiple times for multiple tags)"
            )
        source <- optional $
          (    Arxiv
            <$> strOption
                    (  long "arxiv"
                    <> metavar "ARXIV ID"
                    <> help "Specify ArXiV source"
                    )
          ) <|>
          (     DOI
            <$> strOption
                    (  long "doi"
                    <> metavar "DOI"
                    <> help "Specify DOI source"
                    )
          ) <|>
          (     ISBN
            <$> strOption
                    (  long "isbn"
                    <> metavar "ISBN"
                    <> help "Specify ISBN source"
                    )
          )
        pure $ \path -> Add path $ canonicalize
            Paper { title
                  , authors
                  , year
                  , tags
                  , source
                  }
    get = do
        path <- file
        source <-
          (    Arxiv
            <$> strOption
                    (  long "arxiv"
                    <> metavar "ARXIV ID"
                    <> help "Specify ArXiV source"
                    )
          ) <|>
          (     DOI
            <$> strOption
                    (  long "doi"
                    <> metavar "DOI"
                    <> help "Specify DOI source"
                    )
          )
        tags <- many $ strOption
            (  long "tag"
            <> short 'T'
            <> metavar "TAG"
            <> help "Specify a tag (use multiple times for multiple tags)"
            )
        pure $ Get path source tags
    rm   = Remove <$> file
    mv   = Move <$> argument str (metavar "SOURCE")
                <*> argument str (metavar "TARGET")
    set = do
        path <- file
        newTitle <- optional $ strOption
            (  long "title"
            <> short 't'
            <> metavar "TITLE"
            <> help "Specify title of paper"
            )
        addAuthors <- many $ strOption
            (  long "author"
            <> short 'a'
            <> metavar "NAME"
            <> help "Add an author"
            )
        rmAuthors <- many $ strOption
            (  long "unauthor"
            <> short 'A'
            <> metavar "NAME"
            <> help "Remove an author"
            )
        newYear <- optional $ option auto
            (  long "year"
            <> short 'y'
            <> metavar "YEAR"
            <> help "Specify year of paper"
            )
        addTags <- many $ strOption
            (  long "tag"
            <> short 'T'
            <> metavar "TAG"
            <> help "Add a tag"
            )
        rmTags <- many $ strOption
            (  long "untag"
            <> short 'U'
            <> metavar "TAG"
            <> help "Remove a tag"
            )
        newSource <-
          (     Just
            <$> Just
            <$> Arxiv
            <$> strOption
                    (  long "arxiv"
                    <> metavar "ARXIV ID"
                    <> help "Specify ArXiV source"
                    )
          ) <|>
          (     Just
            <$> Just
            <$> DOI
            <$> strOption
                    (  long "doi"
                    <> metavar "DOI"
                    <> help "Specify DOI source"
                    )
          ) <|>
          (     Just
            <$> Just
            <$> ISBN
            <$> strOption
                    (  long "isbn"
                    <> metavar "ISBN"
                    <> help "Specify ISBN source"
                    )
          ) <|>
          ( flag
                Nothing
                (Just Nothing)
                (  long "unsource"
                <> help "Remove source data entirely"
                )
          )
        pure $ Set path (\Paper { title, authors, year, tags, source } -> canonicalize
                            Paper { title   = fromMaybe title newTitle
                                  , authors = (authors \\ rmAuthors) ++ addAuthors
                                  , year    = fromMaybe year newYear
                                  , tags    = (tags \\ rmTags) ++ addTags
                                  , source  = fromMaybe source newSource
                                  }
                        )
    meta = Meta <$> file
    list = do
        wantKeywords <- many $ strOption
            (  long "title"
            <> short 't'
            <> metavar "KEYWORD"
            <> help "Title should contain KEYWORD"
            )
        wantAuthors <- many $ strOption
            (  long "author"
            <> short 'a'
            <> metavar "NAME"
            <> help "NAME should be an author"
            )
        wantTags <- many $ strOption
            (  long "tag"
            <> short 'T'
            <> metavar "TAG"
            <> help "TAG should be a tag"
            )
        format <- flag
            ListColors
            ListText
            (  long "plain"
            <> help "Give plain text output without colors and formatting"
            )

        -- TODO: Allow "or" searches
        pure $ format (\Paper { title, authors, tags } -> all (`isInfixOf` title  ) wantKeywords
                                                       && all (`elem`      authors) wantAuthors
                                                       && all (`elem`      tags   ) wantTags)
        -- TODO: Allow "or" searches

main :: IO ()
main = execParser opts >>= run
  where
    opts = info (cli <**> helper)
      ( fullDesc
     <> header "pap - a paper manager by Max Vistrup" )

databasePath :: FilePath
databasePath = "pap.json"
writeDatabase :: Database -> IO ()
writeDatabase db = ByteString.writeFile databasePath (encodePretty db)

downloadPdf :: String -> IO ByteString
downloadPdf url = do
    req <- addRequestHeader
        "User-Agent"
        "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"
        <$> parseRequest url
    manager <- newManager tlsManagerSettings
    res <- httpLbs req manager
    pure $ responseBody res

run :: Instruction -> IO ()
run Init = do
    exists <- doesFileExist databasePath
    if exists then
         fail "database already exists"
    else
        writeDatabase initialDatabase
run ins = do
    json <- ByteString.readFile databasePath
    db  <- either fail pure $ eitherDecode json
    case ins of
        Add file paper -> do
            db' <- addPaper db file paper
            writeDatabase db'
            print paper
        AddAuto file source tags -> do
            paper <- case source of
                Arxiv id -> fst <$> getArxivPaper id
                DOI doi  -> getDoiPaper doi
            let paper' = paper { tags = tags }
            db' <- addPaper db file paper'
            writeDatabase db'
            print paper'
        Get file source tags -> do
            (paper, pdf) <- case source of
                Arxiv id -> getArxivPaper id
                DOI doi  -> do
                    paper <- getDoiPaper doi
                    pdf <- getDoiPdfUrl doi
                    pure (paper, pdf)
            let paper' = paper { tags = tags }
            pdfCont <- downloadPdf pdf
            -- TODO: throw error on overwrite
            ByteString.writeFile file pdfCont
            putStrLn (show paper')
            db' <- addPaper db file paper'
            writeDatabase db'
        Remove file -> do
            db' <- removePaper db file
            writeDatabase db'
        Set file f -> do
            db' <- adjustPaper db file f
            writeDatabase db'
            -- TODO: This does two file reads, which is stupid
            run (Meta file)
        Meta file -> do
            paper <- getPaper db file
            print paper
        ListText f -> putStrLn $ listPapers db f
        ListColors f -> printPapersColors db f
        -- TODO: Implement move
