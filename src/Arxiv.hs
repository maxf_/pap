{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Arxiv ( getArxivPaper ) where

import Network.HTTP.Conduit
import Text.XML
import Data.Maybe
import Data.List
import Data.String
import qualified Data.Text as Text
import Data.Text ( Text )
import Database
import qualified Data.ByteString.Lazy as ByteString
import Data.ByteString.Lazy (ByteString)
import qualified Data.Map as Map

getArxivData :: String -> IO ByteString
-- TODO: consistency in string concatenation
getArxivData id = simpleHttp ("http://export.arxiv.org/api/query?id_list=" ++ id)

readMaybe :: Read a => String -> Maybe a
readMaybe s =
    case reads s of
        [(val, "")] -> Just val
        _           -> Nothing

firstJust :: (a -> Maybe b) -> [a] -> Maybe b
firstJust f ls = listToMaybe $ mapMaybe f ls

getArxivPaper :: String -> IO (Paper, String)
getArxivPaper id = do
    xml <- getArxivData id
    doc <- either (fail . show) pure $ parseLBS def xml

    entry     <- getTag "entry" (documentRoot doc)
    title     <- cleanTitle <$> Text.unpack <$> (getTag "title"     entry >>= getCont)
    published <-                Text.unpack <$> (getTag "published" entry >>= getCont)
    authors   <- sequence $  map (>>= (fmap Text.unpack) . getCont)
                          $  getTag "name"
                         <$> getTags "author" entry
    download  <- Text.unpack <$>
                   ( (maybeToIO $
                      find (\Element { elementAttributes } ->
                                Map.lookup "title" elementAttributes
                             == Just "pdf"
                           )
                           (getTags "link" entry)
                     )
                     >>= (\Element { elementAttributes } ->
                        maybeToIO $
                            Map.lookup "href" elementAttributes)
                   )

    year <- maybeToIO
          ( readMaybe
          $ fst
          $ break (\c -> c == '-')
          $ published
          )

    pure (canonicalize Paper { title
                             , year
                             , authors
                             , tags = []
                             , source = Just $ Arxiv id
                             }
         , download)
    where
        arxivError = (fail "bad ArXiV response")
        maybeToIO x = fromMaybe arxivError $ pure <$> x
        getTag tag Element { elementNodes } = maybeToIO $
            firstJust
                (\case NodeElement e @ Element { elementName }
                           | nameLocalName elementName == Text.pack tag -> Just e
                       _                                                -> Nothing)
                elementNodes
        getTags tag Element { elementNodes } =
            [ e
            | node <- elementNodes
            , NodeElement e @ Element { elementName } <- [node]
            , nameLocalName elementName == Text.pack tag
            ]
        getCont Element { elementNodes } = maybeToIO $
            firstJust
                (\case NodeContent c -> Just c
                       _             -> Nothing)
                elementNodes
        cleanTitle title = cleanTitle' $ filter (/= '\n') title
        cleanTitle' (' ':' ':x) = ' ':cleanTitle' x
        cleanTitle' (x:xs)      = x:cleanTitle' xs
        cleanTitle' ""          = ""
